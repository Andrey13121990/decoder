#!env/bin/python

import PySimpleGUI as sg
import pandas as pd
import os


class Decoder:

    def __init__(self):
        pass


    def save_csv(self, df, path, out_file):
            file_format = '.csv'
            out = out_file + file_format
            df.to_csv(os.path.join(path, out))
            print('Sucess import to csv')


    def save_excel(self, df, path, out_file):
            file_format = '.xlsx'
            out = out_file + file_format
            df.to_excel(os.path.join(path, out))
            print('Sucess import to excel')


    def save(self, in_file, path, out_file, format_1, format_2):

        df = pd.read_excel(in_file)

        if format_1:
            self.save_csv(df, path ,out_file)
        else:
            pass
        
        if format_2:
            self.save_excel(df, path ,out_file)
        else:
            pass
            
        if format_1 == False and format_2 == False:
            print('Attention!!! - Choose output format')



class SimpleGui:

    def __init__(self):
        self.GUI = sg
        self.layout = self.field_factory()
        self.window = self.GUI.Window('File Converter', self.layout)


    def field_file (self, text):
        return [self.GUI.Text(text), 
                self.GUI.InputText(), 
                self.GUI.FileBrowse()]


    def field_fold (self, text):     
        return [self.GUI.Text(text), 
                self.GUI.InputText(), 
                self.GUI.FolderBrowse()]


    def field_text(self, text):
        return [self.GUI.Text(text), 
                self.GUI.InputText()]


    def field_checkbox (self, *args):        
        
        results = []
        for arg in args:
            results.append(self.GUI.Checkbox(arg))
        
        return results


    def field_output (self):
        return [self.GUI.Output(size=(60, 30))]


    def field_close (self):
        return [self.GUI.Submit('Start'), 
                self.GUI.Cancel('Close')]        


    def field_factory(self):
        f1 = self.field_file('Input file:')
        f2 = self.field_fold('Save to:')
        f3 = self.field_text('File_name:')
        f4 = self.field_checkbox('.csv', '.xlsx')
        f5 = self.field_output()
        f6 = self.field_close()

        return [f1, f2, f3, f4, f5, f6]


    def run_app(self):

        while True:
            event, values = self.window.read()
            
            if event in (None, 'Exit', 'Close'):
                break

            if event == 'Start':
                
                in_file = values[0]
                path = values[1]
                out_file = values[2]
                is_csv = values[3]
                is_excel = values[4]

                try:
                    Decoder().save(in_file, path, out_file, is_csv, is_excel)
                except:
                    print('Something goes wrong: you need panda help)')


if __name__ == "__main__":
    App = SimpleGui()
    App.run_app()